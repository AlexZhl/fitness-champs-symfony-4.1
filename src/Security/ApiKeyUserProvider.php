<?php

namespace App\Security;

use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class ApiKeyUserProvider implements UserProviderInterface
{
    private $doctrine;

    private $userRepository;

    public function __construct(ManagerRegistry $doctrine, UserRepository $userRepository)
    {
        $this->doctrine = $doctrine;
        $this->userRepository = $userRepository;
    }

    public function getUsernameForApiKey($apiKey)
    {
        $user = $this->doctrine
            ->getRepository(\App\Entity\User::class)
            ->findOneBy(['apiKey' => $apiKey]);

        if (!$user) {
            return null;
        }

        return $user->getUsername();
    }

    public function loadUserByUsername($username)
    {
        $user = $this->userRepository->findOneBy(['username' => $username]);
        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        // this is used for storing authentication in the session
        // but in this example, the token is sent in each request,
        // so authentication can be stateless. Throwing this exception
        // is proper to make things stateless
        throw new UnsupportedUserException();
    }

    public function supportsClass($class)
    {
        return User::class === $class;
    }
}