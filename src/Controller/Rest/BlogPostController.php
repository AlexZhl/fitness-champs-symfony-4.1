<?php

namespace App\Controller\Rest;

use App\Entity\BlogPost;
use App\Service\BlogPostService;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BlogPostController extends FOSRestController
{
    private $blogPostService;

    public function __construct(BlogPostService $blogPostService)
    {
        $this->blogPostService = $blogPostService;
    }

    /**
     * @Rest\Get("/blog")
     */
    public function getBlogPosts(Request $request): View
    {
        $blogPosts = $this->blogPostService->getAllBlogPosts();

        return View::create($blogPosts, Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/blog/{slug}")
     */
    public function getBlogPost(Request $request, string $slug): View
    {
        $blogPost = $this->blogPostService->getBlogPostBySlug($slug);

        return View::create($blogPost, Response::HTTP_OK);
    }

    /**
     * @Rest\Post("/blog")
     * @param Request $request
     * @return View
     */
    public function postBlogPost(Request $request): View
    {
        $blogPost = $this->blogPostService->addBlogPost(
            $request->get('title'),
            $request->get('content'),
            $this->getUser()
        );

        return View::create($blogPost, Response::HTTP_CREATED);
    }
}
