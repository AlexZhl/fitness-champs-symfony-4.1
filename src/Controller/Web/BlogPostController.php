<?php

namespace App\Controller\Web;

use App\Entity\BlogPost;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class BlogPostController extends AbstractController
{
    /**
     * @Route("/blog", name="app_blog")
     */
    public function blog()
    {
        $blog_posts = $this->getDoctrine()
            ->getRepository(BlogPost::class)
            ->findBy(['status' => BlogPost::STATUS_PUBLISHED]);

        return $this->render('blog/blog.html.twig', [
            'blog_posts' => $blog_posts,
        ]);
    }

    /**
     * @Route("/blog/{slug}", name="app_blog_details")
     */
    public function blogDetails($slug)
    {
        $blog_post = $this->getDoctrine()
            ->getRepository(BlogPost::class)
            ->findOneBy(['slug' => $slug, 'status' => BlogPost::STATUS_PUBLISHED]);

        if (!$blog_post) {
            throw new NotFoundHttpException();
        }

        return $this->render('blog/blog_details.html.twig', [
            'blog_post' => $blog_post,
        ]);
    }
}
