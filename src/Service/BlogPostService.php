<?php

namespace App\Service;

use App\Entity\BlogPost;
use App\Entity\User;
use App\Repository\BlogPostRepositoryInterface;
use App\Repository\UserRepository;
use App\Utils\Slugger;
use Doctrine\ORM\EntityNotFoundException;

/**
 * @package App\Service
 */
final class BlogPostService
{
    /**
     * @var BlogPostRepositoryInterface
     */
    private $blogPostRepository;

    /**
     * @var UserRepository
     */
    private $userRepostory;

    public function __construct(
        BlogPostRepositoryInterface $blogPostRepository,
        UserRepository $userRepository
    ) {
        $this->blogPostRepository = $blogPostRepository;
        $this->userRepostory = $userRepository;
    }

    /**
     * @return array|null
     */
    public function getAllBlogPosts(): ?array
    {
        return $this->blogPostRepository->findAll();
    }

    /**
     * @return array|null
     */
    public function getBlogPostBySlug(string $slug): BlogPost
    {
        return $this->blogPostRepository->findOneBy(['slug' => $slug]);
    }

    public function addBlogPost(string $title, string $content, User $author, int $status = BlogPost::STATUS_DRAFT): BlogPost
    {
        $blogPost = new BlogPost();
        $blogPost->setTitle($title);
        $blogPost->setContent($content);
        $blogPost->setStatus($status);
        $blogPost->setSlug(Slugger::slugify($blogPost->getTitle()));
        $blogPost->setAuthor($author);
        $this->blogPostRepository->save($blogPost);

        return $blogPost;
    }
}