<?php

namespace App\Repository;

use App\Entity\BlogPost;

interface BlogPostRepositoryInterface
{
    public function find(int $blogPostId): ?BlogPost;

    public function findAll(): array;

    public function save(BlogPost $blogPost): void;

    public function delete(BlogPost $blogPost): void;
}
