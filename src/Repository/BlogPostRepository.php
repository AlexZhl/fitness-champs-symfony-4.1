<?php

namespace App\Repository;

use App\Entity\BlogPost;
use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\EntityManagerInterface;

class BlogPostRepository implements BlogPostRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ObjectRepository
     */
    private $objectRepository;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->objectRepository = $this->entityManager->getRepository(BlogPost::class);
    }

    public function find(int $blogPostId): ?BlogPost
    {
        return $this->objectRepository->find($blogPostId);
    }

    public function save(BlogPost $blogPost): void
    {
        $this->entityManager->persist($blogPost);
        $this->entityManager->flush();
    }

    public function delete(BlogPost $blogPost): void
    {
        $this->entityManager->remove($blogPost);
        $this->entityManager->flush();
    }

    public function findAll(): array
    {
        return $this->objectRepository->findAll();
    }

    public function findOneBy($criteria): ?BlogPost
    {
        return $this->objectRepository->findOneBy($criteria);
    }
}
